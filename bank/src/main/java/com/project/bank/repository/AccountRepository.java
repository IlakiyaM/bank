package com.project.bank.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.bank.entity.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
	    Account findByAccountNumberEquals(Long accountNumber);
	    Optional<Account> findByAccountNumber(Long accountNumber);
	    List<Account> findAll();
}
