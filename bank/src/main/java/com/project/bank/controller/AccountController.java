package com.project.bank.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import javax.xml.ws.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.bank.dto.TransferDetails;
import com.project.bank.entity.Account;
import com.project.bank.entity.Transaction;
import com.project.bank.service.AccountService;

@RestController
@RequestMapping("/api/account")
public class AccountController {
	@Autowired private AccountService accountService;
    @PostMapping("/register")
    public String create(@RequestBody Account account) {
        accountService.save(account);
        return "Successfully Registered";
    }

    @GetMapping("/display")
    public List<Account> display() {
        return accountService.findAll();
    }
}
