package com.project.bank.controller;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.project.bank.dto.TransferDetails;
import com.project.bank.entity.Transaction;
import com.project.bank.service.TransactionService;


@RestController
@RequestMapping("/api/account")
public class TransactionController {

	@Autowired
	TransactionService transactionService;
	
	@PostMapping("/fundtransfer")
	public String transfer(@RequestBody TransferDetails transferDetails) {

	           String response= transactionService.transfer(transferDetails);
	           return response;
	}
	@GetMapping("/statements")
	public List<Transaction> getStatement(@RequestParam String txDateTime){
	           return transactionService.getStatement(txDateTime);
	}
	
}
