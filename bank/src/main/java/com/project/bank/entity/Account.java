package com.project.bank.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name="account")
public class Account implements Serializable{

	@Id
	@Column(name="account_id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long accountNumber;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
   	
	@Column(name="account_balance")
	private Double accountBalance;
	
	@Column(name="pan")
	private String panNumber;
	
	@Column(name="email")
	private String emailID;
	
	public double getAccountBalance() {
		return accountBalance;
	}

	public String getPanNumber() {
		return panNumber;
	}

	public void setPanNumber(String panNumber) {
		this.panNumber = panNumber;
	}

	public String getEmailID() {
		return emailID;
	}

	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

/*	public Long getAccountNumber() {
		return accountNumber;
	}
*/
/*	public void setAccountNumber(Long accountNumber) {
		this.accountNumber = accountNumber;
	} */

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
