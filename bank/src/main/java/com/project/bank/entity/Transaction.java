package com.project.bank.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "transaction")
public class Transaction implements Serializable {
	@Id
	@Column(name = "transaction_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long transactionId;

	@Column(name = "from_account")
	@NotNull(message = "From accountNumber must be specified.")
	private Long fromAccountNumber;

	public Long getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(Long fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	@Column(name = "to_account")
	@NotNull(message = "To accountNumber must be specified.")
	private Long toAccountNumber;

	@Column(name = "tx_amount")
	@NotNull(message = "Amount to be transferred must be specified.")
	private Double accountBalance;

	@Column(name = "date_time")
	private String txDateTime;

	public String getTxDateTime() {
		return txDateTime;
	}

	public void setTxDateTime(String txDateTime) {
		this.txDateTime = txDateTime;
	}
    public Transaction(){}
	public Transaction(Long fromAccountNumber, Long toAccountNumber, Double transferAmount, String date) {
		this.fromAccountNumber = fromAccountNumber;
		this.toAccountNumber = toAccountNumber;
		this.accountBalance = transferAmount;
		this.txDateTime = date;
	}
	
	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getToAccountNumber() {
		return toAccountNumber;
	}

	public void setToAccountNumber(Long toAccountNumber) {
		this.toAccountNumber = toAccountNumber;
	}

	public Double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(Double accountBalance) {
		this.accountBalance = accountBalance;
	}

}