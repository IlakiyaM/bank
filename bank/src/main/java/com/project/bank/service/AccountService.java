package com.project.bank.service;

import java.util.List;
import com.project.bank.entity.Account;


public interface AccountService {
	List<Account> findAll();

	void save(Account account);

}
