package com.project.bank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.project.bank.dto.TransferDetails;
import com.project.bank.entity.Account;
import com.project.bank.entity.Transaction;
import com.project.bank.repository.AccountRepository;
import com.project.bank.repository.TransactionRepository;
import com.project.bank.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;

	public void save(Account account) {
		accountRepository.save(account);
		//accountRepository.findByAccountNumberEquals(account.getAccountNumber());
	}

	public List<Account> findAll() {
		return accountRepository.findAll();
	}

	public Account findByAccountNumber(Long accountNumber) {
		Account account = accountRepository.findByAccountNumberEquals(accountNumber);
		return account;
	}

	/*
	 * @Override public ResponseEntity<Object> transfer(TransferDetails
	 * transferDetails) { List<Account> accountEntities = new ArrayList<>();
	 * Account fromAccountEntity = null; Account toAccountEntity = null;
	 * Optional<Account> fromAccountEntityOpt = accountRepository
	 * .findByAccountNumber(transferDetails.getFromAccountNumber());
	 * Optional<Account> toAccountEntityOpt = accountRepository
	 * .findByAccountNumber(transferDetails.getToAccountNumber()); if
	 * (fromAccountEntityOpt.isPresent() && toAccountEntityOpt.isPresent()) {
	 * fromAccountEntity = fromAccountEntityOpt.get(); toAccountEntity =
	 * toAccountEntityOpt.get(); if (fromAccountEntity.getAccountBalance() <
	 * transferDetails.getTransferAmount()) { return
	 * ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Insufficient Funds."
	 * ); } else { synchronized (this) { fromAccountEntity
	 * .setAccountBalance(fromAccountEntity.getAccountBalance() -
	 * transferDetails.getTransferAmount());
	 * accountEntities.add(fromAccountEntity);
	 * 
	 * toAccountEntity .setAccountBalance(toAccountEntity.getAccountBalance() +
	 * transferDetails.getTransferAmount());
	 * accountEntities.add(toAccountEntity);
	 * 
	 * accountRepository.saveAll(accountEntities);
	 * 
	 * Transaction fromTransaction = transactionRepository .save(new
	 * Transaction(transferDetails.getFromAccountNumber(),
	 * transferDetails.getToAccountNumber(),
	 * transferDetails.getTransferAmount(), new
	 * Timestamp(System.currentTimeMillis())));
	 * transactionRepository.save(fromTransaction);
	 * 
	 * }
	 * 
	 * return
	 * ResponseEntity.status(HttpStatus.OK).body("Success: Amount transferred "
	 * ); } } else { return ResponseEntity.status(HttpStatus.NOT_FOUND)
	 * .body("To Account Number " + transferDetails.getToAccountNumber() +
	 * " not found."); } }
	 * 
	 * @Override public List<Transaction> getStatement(LocalDate txDateTime) {
	 * System.out.println("Inside get statements"); List<Transaction>
	 * transactionDetails=
	 * transactionRepository.findByTxDateTimeStartingWith(txDateTime);
	 * System.out.println("After get statements query");
	 * transactionDetails.forEach(System.out::println); return
	 * transactionDetails; }
	 */
	/*
	 * @Override public AccountStatement getStatement(Long accountNumber) {
	 * Account account =
	 * accountRepository.findByAccountNumberEquals(accountNumber); return new
	 * AccountStatement(account.getAccountBalance(),transactionRepository.
	 * findByAccountNumberEquals(accountNumber)); }
	 */
}
