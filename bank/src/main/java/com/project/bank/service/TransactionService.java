package com.project.bank.service;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.http.ResponseEntity;

import com.project.bank.dto.TransferDetails;
import com.project.bank.entity.Transaction;

public interface TransactionService {
	
		String transfer(TransferDetails transferDetails);

		List<Transaction> getStatement(String txDateTime);
	}


