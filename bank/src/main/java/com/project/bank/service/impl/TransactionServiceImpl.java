package com.project.bank.service.impl;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.project.bank.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.project.bank.dto.TransferDetails;
import com.project.bank.entity.Account;
import com.project.bank.entity.Transaction;
import com.project.bank.repository.AccountRepository;
import com.project.bank.repository.TransactionRepository;

@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	TransactionRepository transactionRepository;
	@Autowired
	AccountRepository accountRepository;

	@Override
	public String transfer(TransferDetails transferDetails) {
		List<Account> accountEntities = new ArrayList<>();
		Account fromAccountEntity = null;
		Account toAccountEntity = null;
		Optional<Account> fromAccountEntityOpt = accountRepository
				.findByAccountNumber(transferDetails.getFromAccountNumber());
		Optional<Account> toAccountEntityOpt = accountRepository
				.findByAccountNumber(transferDetails.getToAccountNumber());
		if (fromAccountEntityOpt.isPresent() && toAccountEntityOpt.isPresent()) {
			fromAccountEntity = fromAccountEntityOpt.get();
			toAccountEntity = toAccountEntityOpt.get();
			if (fromAccountEntity.getAccountBalance() < transferDetails.getTransferAmount()) {
				//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Insufficient Funds.");
				return "Insufficient Funds.";
			} 
			else if (transferDetails.getTransferAmount()<= 0){
				//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Enter a valid amount");
				return "Enter a valid amount";
			}
			  else if (transferDetails.getFromAccountNumber().equals(transferDetails.getToAccountNumber())) {
				//return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Can't transfer funds to the same account");
				return "Can't transfer funds to the same account";
			} else {
				synchronized (this) {
					fromAccountEntity.setAccountBalance(
							fromAccountEntity.getAccountBalance() - transferDetails.getTransferAmount());
					accountEntities.add(fromAccountEntity);

					toAccountEntity.setAccountBalance(
							toAccountEntity.getAccountBalance() + transferDetails.getTransferAmount());
					accountEntities.add(toAccountEntity);

					accountRepository.saveAll(accountEntities);
					Transaction fromTransaction = transactionRepository
							.save(new Transaction(transferDetails.getFromAccountNumber(),
									transferDetails.getToAccountNumber(), transferDetails.getTransferAmount(),
									(new Timestamp(System.currentTimeMillis())).toString()));
					transactionRepository.save(fromTransaction);

				}

				//return ResponseEntity.status(HttpStatus.OK).body("Success: Amount transferred ");
				return "Success";
			}
		} else {
			return " Account Number " + transferDetails.getFromAccountNumber() + " not found.";
		}
	}

	@Override
	public List<Transaction> getStatement(String txDateTime) {
		System.out.println("Inside get statements");
		List<Transaction> transactionDetails = transactionRepository.findByTxDateTimeContaining(txDateTime);
		System.out.println("After get statements query");
		// transactionDetails.forEach(System.out::println);
		return transactionDetails;
	}

}
